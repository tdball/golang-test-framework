
# Live Coverage
![coverage](https://gitlab.com/tdball/golang-test-framework/badges/develop/coverage.svg)

### [Development Branch Coverage HTML Report](https://tdball.gitlab.io/golang-test-framework/develop)

![coverage](https://gitlab.com/tdball/golang-test-framework/badges/staging/coverage.svg)

### [Staging Branch Coverage HTML Report](https://tdball.gitlab.io/golang-test-framework/staging)

![coverage](https://gitlab.com/tdball/golang-test-framework/badges/main/coverage.svg)

### [Production Branch Coverage HTML Report](https://tdball.gitlab.io/golang-test-framework/main)

# Usage
Testing script is located at `scripts/test.sh`. These are used by GitLab CI/CD and will generate html reporting for code coverage.

### Local Testing
export the required environment variables for the script to work locally
```
export CI_COMMIT_BRANCH="feature"
export WORKING_DIR="."
```

Then execute the test script

```
scripts/test.sh
```
The test results can then be viewed locally by browsing to the HTML report within the `test_coverage/$CI_COMMIT_BRANCH/` folder


