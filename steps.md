# Task 1: Design a testing framework
* Initial implementation, leverage native `go test`

# Task 2: Test Additions
* Add edge case to `TestCalculate` for expected errors on exponential operations

# Task 3: CI/CD Integration
* Installed docker runner container on development machine
  * Configure from within following https://docs.gitlab.com/runner/install/docker.html
* created .gitlab-ci.yml
* pushed to gitlab remote, job ran and failed due to missing go binary
* create dockerfile for custom runner container
```
docker build . -t golang-testing/gitlab-runner
```
* repeat test execution
* CI/CD missing tag for `golang`, did not run in the expected local runner
* Updated CI/CD with `golang` tag
* Container failed due to malformed go.mod
  * Updated go.mod to declare `1.21` instead of `1.21.1`
* Runner execution of tests successful
  * https://gitlab.com/tdball/golang-test-framework/-/jobs/6483672602

# Task 4: Test Result Reporting
* Emit coverage report using go test's `-cover` flag
* Leverage GitLab CI/CD to parse coverage
* First attempt failed, line does not start with coverage.
  * Consider emitting report and pointing gitlab at the report
  * Consider pulling in `https://github.com/boumenot/gocover-cobertura` as a dependency to emit xml reports

# Task 5: Documentation
* Created basic documentation, referencing usage and the live coverage data hosted on GitLab Pages