#!/usr/bin/env bash

# Leveraging built in go test command to emit simple test coverage
# and emitting a test profile for deeper parsing
COVERAGE_DIR="${WORKING_DIR}/test_coverage"
BRANCH_DIR="${COVERAGE_DIR}/${CI_COMMIT_BRANCH}"
mkdir -p "${BRANCH_DIR}"
go test ./... -cover --coverprofile="${BRANCH_DIR}/coverage.out"
go tool cover -html="${BRANCH_DIR}/coverage.out" -o "${BRANCH_DIR}/index.html"